﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Deconv_svertk
{
    class Model
    {
        const int N = 512; //кол-во отсчётов

        int[] M  = new int[2]; //Число отсчетов АКП
        double[,] lamb; //двумерная лямбда, для метода СВД
        double[] lambd;//одномерный массив, хранящий собственные числа
        double[,] u;//не используемые массивы
        double[,] v;//не используемые массивы

        double[,] R; //АК-матрица

        double[] w; //частоты
        double[] rxx; // отсчеты АКП
        double[] ф; //начальная фаза

        double[] x; //исходный сигнал

        double cordR; // обусловленность матрицы
              
       
        double[] d = new double[2];

        Random rand = new Random();

        public Model()
        {


          
            w = new double[2];
            ф= new double[2];
       
            x = new double[N];
                   

            //----------------------------------------------------------------------------------
             
  
        }

        private void DoubleArray(double[] X, int n, string str)
        {
            string[] s = str.Split();
            for (int i = 0; i < n; i++)
            {
                X[i] = Convert.ToDouble(s[i]);
            }
        }

        private void IntArray(int[] X, int n, string str)
        {
            string[] s = str.Split();
            for (int i = 0; i < n; i++)
            {
                X[i] = Convert.ToInt16(s[i]);
            }
        }

        internal void Setw(TextBox t1, TextBox t2)
        {
            DoubleArray(w, 2, t1.Text + " " + t2.Text);
        }

        internal void Setф(TextBox t1, TextBox t2)
        {
            DoubleArray(ф, 2, t1.Text + " " + t2.Text);
        }
        private void InttNoise(double[] X, string str)
        {
            //основная функция; для числа
            string[] s = str.Split();
            s[1] = " ";
            X[0] = Convert.ToDouble(s[0]);
        }

        private void IntM(int[] X, string str)
        {
            //основная функция; для числа
            string[] s = str.Split();
            s[1] = " ";
            X[0] = Convert.ToInt16(s[0]);
        }
        internal void Setd(TextBox t1)
        {
            InttNoise(d, t1.Text + " ");
        }

        internal void SetM(TextBox t1)
        {
            IntM(M, t1.Text + " ");
        }

       


      



        internal void Signal() //создание сигнала
        {

            for (int t = 0; t < N; t++)//заполнение массива значений х
            {
                x[t] = 0;
                for (int i = 0; i < 2; i++)
                {
                    x[t] += Math.Sin(2*Math.PI*w[i]*t + ф[i]);
                }
            }

 
           
            double f;

            double[] n = new double[N];
            for (int t = 0; t < N; t++)
            {
                f = 0;
                for (int j = 0; j < 20; j++)
                {
                    f += Convert.ToDouble(rand.Next(-2000, 2001)) / 2000;
                }
                n[t] = f / 20;
            }
            double shumy = 0;
            for (int t = 0; t < N; t++)
            {
                shumy += x[t] * x[t];
            }

            double shumn = 0;
            for (int t = 0; t < N; t++)
            {
                shumn += n[t] * n[t];
            }
            d[0] /= 100;
            //подсчёт коэфициента альфа
            double alfa = Math.Sqrt(d[0] * shumy / shumn);

            for (int t = 0; t < N; t++)
            {
                x[t] = x[t] + (alfa * n[t]);
            }

            //считаем отсчеты АКП:
            rxx = new double[M[0]];

            for (int i = 0; i < M[0]; i++)
            {
                for (int j = 0; j < N-i; j++)
			    {
                    rxx[i] +=  x[j] * x[j + i];
			    }
                
            }



            //составляем АК-матрицу
              R = new double[M[0], M[0]];
              lamb = new double[M[0], M[0]];
              u = new double[M[0], M[0]];
              v = new double[M[0], M[0]];
            lambd = new double[M[0]];

           	for (int i = 0; i < M[0]; i++)//Заполнение массива матрицы
		{
			for (int j = 0; j < M[0]; j++)
			{
				R[i, j] = (rxx[Math.Abs(i - j)]);
			}
		}
        
          int it =  svd_hestenes(M[0], M[0], R);

          for (int i = 0; i < M[0]; i++)            //запишем двумерный массив в одномерный
          {
              lambd[i] = lamb[i, i];
          }

          cordR = lambd[0] / lambd[M[0]-1];
        }


        
       



        //нахождение собственных векторов

      int svd_hestenes(int m_m, int n_n, double[,] a)
{
double thr=1E-4f, nul=1E-16f;
int n,m,i,j,l,k,iter, ipn,ll,kk;
double alfa,betta,hamma,eta,t,cos0,sin0,buf,s ;
            bool lort;
  n = n_n;
  m = m_m;
	for(i=0;i<n;i++)
		  { 
		for(j=0;j<n;j++)
			if(i==j) v[i,j]=1;
			else v[i,j]=0;
			}
	for(i=0;i<m;i++)
		  {     
		for(j=0;j<n;j++)
					 {
			u[i,j]=a[i,j];
					  }
			 }

	iter=0;
	while(true)
		{
		 lort=false;
		iter++;
		for(l=0;l<n-1;l++)
			for(k=l+1;k<n;k++)
				{
				alfa=0; betta=0; hamma=0;
				for(i=0;i<m;i++)
					{
						 ipn=i*n;
						 ll=ipn+l;
						 kk=ipn+k;
					alfa += u[i,l]*u[i,l];
					betta+= u[i,k]*u[i,k];
					hamma+= u[i,l]*u[i,k];
					}

				if( Math.Sqrt(alfa*betta) < nul )	continue;
				if(Math.Abs(hamma)/Math.Sqrt(alfa*betta)<thr) continue;

				lort=true;
				 eta=(betta-alfa)/(2*hamma);
				 t=(double)((eta/Math.Abs(eta))/(Math.Abs(eta)+Math.Sqrt(1+eta*eta)));
				 cos0=(double)(1/Math.Sqrt(1+t*t));
				 sin0=t*cos0;

				for(i=0;i<m;i++)
					{
					
					buf=u[i,l]*cos0-u[i,k]*sin0;
					u[i,k]=u[i,l]*sin0+u[i,k]*cos0;
					u[i,l]=buf;

					if(i>=n) continue;
					buf=v[i,l]*cos0-v[i,k]*sin0;
					v[i,k]=v[i,l]*sin0+v[i,k]*cos0;
					v[i,l]=buf;
					}
				}

		if(!lort) break;
		}

	for(i=0;i<n;i++)
		{
			  s=0;
		for(j=0;j<m;j++)	s+=u[j,i]*u[j,i];
		s=Math.Sqrt(s);
		lamb[i,i]=s;
			if( s < nul )	continue;
		for(j=0;j<m;j++)	u[j,i]/=s;
		}
//======= Sortirovka ==============
	for(i=0;i<n-1;i++)
		for(j=i;j<n;j++)
            if (lamb[i,i] < lamb[j,j])
				{
                    s = lamb[i,i]; 
                    lamb[i,i] = lamb[j,j];
                    lamb[j,j] = s;
				  for(k=0;k<m;k++)
				  {
                      s= u[k,i]; 
                      u[k,i]=u[k,j]; 
                      u[k,j]=s;
                  }
				  for(k=0;k<n;k++)
				  { 
                      s= v[k,i]; 
                      v[k,i]=v[k,j]; 
                      v[k,j]=s;
                  }
				}
    return iter;
}
        /*
void svd_hestenes(int m_m, int n_n, double[,] a)
{
double thr=1E-4f, nul=1E-16f;
int n,m,i,j,l,k,iter, ipn,ll,kk;
double alfa,betta,hamma,eta,t,cos0,sin0,buf,s ;
            bool lort;
  n = n_n;
  m = m_m;
	for(i=0;i<n;i++)
		  { ipn=i*n;
		for(j=0;j<n;j++)
			if(i==j) v[ipn+j]=1;
			else v[ipn+j]=0;
			}
	for(i=0;i<m;i++)
		  {       ipn=i*n;
		for(j=0;j<n;j++)
					 {
			u[ipn+j]=a[ipn+j];
					  }
			 }

	iter=0;
	while(true)
		{
		 lort=false;
		iter++;
		for(l=0;l<n-1;l++)
			for(k=l+1;k<n;k++)
				{
				alfa=0; betta=0; hamma=0;
				for(i=0;i<m;i++)
					{
						 ipn=i*n;
						 ll=ipn+l;
						 kk=ipn+k;
					alfa += u[ll]*u[ll];
					betta+= u[kk]*u[kk];
					hamma+= u[ll]*u[kk];
					}

				if( Math.Sqrt(alfa*betta) < nul )	continue;
				if(Math.Abs(hamma)/Math.Sqrt(alfa*betta)<thr) continue;

				lort=true;
				 eta=(betta-alfa)/(2*hamma);
				 t=(double)((eta/Math.Abs(eta))/(Math.Abs(eta)+Math.Sqrt(1+eta*eta)));
				 cos0=(double)(1/Math.Sqrt(1+t*t));
				 sin0=t*cos0;

				for(i=0;i<m;i++)
					{
													 ipn=i*n;
					buf=u[ipn+l]*cos0-u[ipn+k]*sin0;
					u[ipn+k]=u[ipn+l]*sin0+u[ipn+k]*cos0;
					u[ipn+l]=buf;

					if(i>=n) continue;
					buf=v[ipn+l]*cos0-v[ipn+k]*sin0;
					v[ipn+k]=v[ipn+l]*sin0+v[ipn+k]*cos0;
					v[ipn+l]=buf;
					}
				}

		if(!lort) break;
		}

	for(i=0;i<n;i++)
		{
			  s=0;
		for(j=0;j<m;j++)	s+=u[j*n+i]*u[j*n+i];
		s=Math.Sqrt(s);
		lamb[i]=s;
			if( s < nul )	continue;
		for(j=0;j<m;j++)	u[j*n+i]/=s;
		}
//======= Sortirovka ==============
	for(i=0;i<n-1;i++)
		for(j=i;j<n;j++)
            if (lamb[i] < lamb[j])
				{
                    s = lamb[i]; lamb[i] = lamb[j]; lamb[j] = s;
				  for(k=0;k<m;k++)
				  { s= u[i+k*n]; u[i+k*n]=u[j+k*n]; u[j+k*n]=s;}
				  for(k=0;k<n;k++)
				  { s= v[i+k*n]; v[i+k*n]=v[j+k*n]; v[j+k*n]=s;}
				}
}*/ //исходный метод

        //================================//







        internal int GetN() { return N; }

        internal int GetM() { return M[0]; }

        internal double GetcordR() { return cordR; }

        internal double[] GetX() { return x; }

        internal double[] GetRxx() { return rxx; }
        internal double[] GetLamb() { return lambd; }
      

    }
}
