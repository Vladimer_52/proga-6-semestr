﻿namespace Deconv_svertk
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tM = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.M = new System.Windows.Forms.TextBox();
            this.Tnmax = new System.Windows.Forms.TextBox();
            this.d = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ф2 = new System.Windows.Forms.TextBox();
            this.ф1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.w2 = new System.Windows.Forms.TextBox();
            this.w1 = new System.Windows.Forms.TextBox();
            this.Reseach_timer = new System.Windows.Forms.Timer(this.components);
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Draw = new System.Windows.Forms.Button();
            this.bar = new System.Windows.Forms.StatusStrip();
            this.Status = new System.Windows.Forms.ToolStripStatusLabel();
            this.Sfunc = new System.Windows.Forms.ToolStripStatusLabel();
            this.Skv = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.reseach = new System.Windows.Forms.Button();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.bar.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.groupBox3);
            this.panel4.Controls.Add(this.groupBox2);
            this.panel4.Controls.Add(this.groupBox1);
            this.panel4.Location = new System.Drawing.Point(1412, 277);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(204, 704);
            this.panel4.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tM);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.M);
            this.groupBox3.Controls.Add(this.Tnmax);
            this.groupBox3.Controls.Add(this.d);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Window;
            this.groupBox3.Location = new System.Drawing.Point(32, 428);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(126, 263);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // tM
            // 
            this.tM.AutoSize = true;
            this.tM.Location = new System.Drawing.Point(9, 182);
            this.tM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tM.Name = "tM";
            this.tM.Size = new System.Drawing.Size(22, 20);
            this.tM.TabIndex = 2;
            this.tM.Text = "M";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 108);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "nmax, %";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "d,%";
            // 
            // M
            // 
            this.M.Location = new System.Drawing.Point(8, 206);
            this.M.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.M.Name = "M";
            this.M.Size = new System.Drawing.Size(102, 26);
            this.M.TabIndex = 0;
            this.M.Text = "5";
            // 
            // Tnmax
            // 
            this.Tnmax.Location = new System.Drawing.Point(8, 132);
            this.Tnmax.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Tnmax.Name = "Tnmax";
            this.Tnmax.Size = new System.Drawing.Size(102, 26);
            this.Tnmax.TabIndex = 0;
            this.Tnmax.Text = "100";
            // 
            // d
            // 
            this.d.Location = new System.Drawing.Point(8, 55);
            this.d.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.d.Name = "d";
            this.d.Size = new System.Drawing.Size(102, 26);
            this.d.TabIndex = 0;
            this.d.Text = "0";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ф2);
            this.groupBox2.Controls.Add(this.ф1);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Window;
            this.groupBox2.Location = new System.Drawing.Point(32, 232);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(126, 186);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ф";
            // 
            // ф2
            // 
            this.ф2.Location = new System.Drawing.Point(8, 132);
            this.ф2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ф2.Name = "ф2";
            this.ф2.Size = new System.Drawing.Size(102, 26);
            this.ф2.TabIndex = 0;
            this.ф2.Text = "3";
            // 
            // ф1
            // 
            this.ф1.Location = new System.Drawing.Point(8, 65);
            this.ф1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ф1.Name = "ф1";
            this.ф1.Size = new System.Drawing.Size(102, 26);
            this.ф1.TabIndex = 0;
            this.ф1.Text = "2,2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.w2);
            this.groupBox1.Controls.Add(this.w1);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.groupBox1.Location = new System.Drawing.Point(32, 22);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(126, 186);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "w";
            // 
            // w2
            // 
            this.w2.Location = new System.Drawing.Point(8, 132);
            this.w2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.w2.Name = "w2";
            this.w2.Size = new System.Drawing.Size(102, 26);
            this.w2.TabIndex = 0;
            this.w2.Text = "0,4";
            // 
            // w1
            // 
            this.w1.Location = new System.Drawing.Point(8, 65);
            this.w1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.w1.Name = "w1";
            this.w1.Size = new System.Drawing.Size(102, 26);
            this.w1.TabIndex = 0;
            this.w1.Text = "0,2";
            // 
            // Reseach_timer
            // 
            this.Reseach_timer.Interval = 10;
            this.Reseach_timer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chart1
            // 
            chartArea5.AxisX.Interval = 30D;
            chartArea5.AxisX.Minimum = 0D;
            chartArea5.AxisX.Title = "N";
            chartArea5.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea5.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            chartArea5.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.chart1.Legends.Add(legend5);
            this.chart1.Location = new System.Drawing.Point(32, 25);
            this.chart1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series5.BorderWidth = 2;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.Legend = "Legend1";
            series5.LegendText = "X";
            series5.Name = "Series1";
            this.chart1.Series.Add(series5);
            this.chart1.Size = new System.Drawing.Size(1350, 320);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            // 
            // Draw
            // 
            this.Draw.Location = new System.Drawing.Point(24, 38);
            this.Draw.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Draw.Name = "Draw";
            this.Draw.Size = new System.Drawing.Size(152, 48);
            this.Draw.TabIndex = 0;
            this.Draw.Text = "Demo";
            this.Draw.UseVisualStyleBackColor = true;
            this.Draw.Click += new System.EventHandler(this.Draw_Click);
            // 
            // bar
            // 
            this.bar.AllowMerge = false;
            this.bar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Status,
            this.Sfunc,
            this.Skv,
            this.toolStripStatusLabel1});
            this.bar.Location = new System.Drawing.Point(0, 1038);
            this.bar.Name = "bar";
            this.bar.Padding = new System.Windows.Forms.Padding(2, 0, 14, 0);
            this.bar.Size = new System.Drawing.Size(1635, 22);
            this.bar.TabIndex = 5;
            this.bar.Text = "statusStrip1";
            // 
            // Status
            // 
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(0, 17);
            // 
            // Sfunc
            // 
            this.Sfunc.Name = "Sfunc";
            this.Sfunc.Size = new System.Drawing.Size(0, 17);
            // 
            // Skv
            // 
            this.Skv.Name = "Skv";
            this.Skv.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox4
            // 
            this.groupBox4.AutoSize = true;
            this.groupBox4.Controls.Add(this.reseach);
            this.groupBox4.Controls.Add(this.Draw);
            this.groupBox4.Location = new System.Drawing.Point(1412, 60);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(201, 182);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            // 
            // reseach
            // 
            this.reseach.Enabled = false;
            this.reseach.Location = new System.Drawing.Point(24, 106);
            this.reseach.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.reseach.Name = "reseach";
            this.reseach.Size = new System.Drawing.Size(152, 48);
            this.reseach.TabIndex = 0;
            this.reseach.Text = "Исследование";
            this.reseach.UseVisualStyleBackColor = true;
            this.reseach.Click += new System.EventHandler(this.reseach_Click);
            // 
            // chart2
            // 
            chartArea6.AxisX.Interval = 1D;
            chartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea6.AxisX.Minimum = 0D;
            chartArea6.AxisX.MinorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea6.AxisX.Title = "M";
            chartArea6.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea6.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea6.AxisY.MinorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea6.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chart2.Legends.Add(legend6);
            this.chart2.Location = new System.Drawing.Point(32, 354);
            this.chart2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chart2.Name = "chart2";
            this.chart2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series6.BorderWidth = 2;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series6.Legend = "Legend1";
            series6.LegendText = "Rxx";
            series6.Name = "Series1";
            this.chart2.Series.Add(series6);
            this.chart2.Size = new System.Drawing.Size(694, 320);
            this.chart2.TabIndex = 2;
            this.chart2.Text = "chart1";
            // 
            // chart3
            // 
            chartArea7.AxisX.LineWidth = 2;
            chartArea7.AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea7.AxisX.Minimum = 0D;
            chartArea7.AxisX.Title = "M";
            chartArea7.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea7.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            chartArea7.AxisX2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea7.AxisY.LineWidth = 2;
            chartArea7.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea7.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.chart3.Legends.Add(legend7);
            this.chart3.Location = new System.Drawing.Point(735, 354);
            this.chart3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chart3.Name = "chart3";
            this.chart3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series7.BorderWidth = 2;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series7.Legend = "Legend1";
            series7.LegendText = "л";
            series7.Name = "Series1";
            series7.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            series7.YValuesPerPoint = 2;
            this.chart3.Series.Add(series7);
            this.chart3.Size = new System.Drawing.Size(646, 320);
            this.chart3.TabIndex = 2;
            this.chart3.Text = "chart1";
            // 
            // chart4
            // 
            chartArea8.AxisX.Interval = 6D;
            chartArea8.AxisX.Minimum = 0D;
            chartArea8.AxisX.Title = "n,%";
            chartArea8.AxisX.TitleAlignment = System.Drawing.StringAlignment.Far;
            chartArea8.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            chartArea8.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea8);
            legend8.Name = "Legend1";
            this.chart4.Legends.Add(legend8);
            this.chart4.Location = new System.Drawing.Point(32, 702);
            this.chart4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chart4.Name = "chart4";
            this.chart4.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series8.BorderWidth = 2;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series8.Legend = "Legend1";
            series8.LegendText = "1/cond(R)";
            series8.Name = "Series1";
            series8.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chart4.Series.Add(series8);
            this.chart4.Size = new System.Drawing.Size(1350, 320);
            this.chart4.TabIndex = 2;
            this.chart4.Text = "chart1";
            this.chart4.Click += new System.EventHandler(this.chart4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(1635, 1060);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.bar);
            this.Controls.Add(this.chart3);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart4);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.panel4);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "N10";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.panel4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.bar.ResumeLayout(false);
            this.bar.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox w2;
        private System.Windows.Forms.TextBox w1;
        private System.Windows.Forms.Timer Reseach_timer;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button Draw;
        private System.Windows.Forms.StatusStrip bar;
        private System.Windows.Forms.ToolStripStatusLabel Sfunc;
        private System.Windows.Forms.ToolStripStatusLabel Skv;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel Status;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox ф2;
        private System.Windows.Forms.TextBox ф1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Tnmax;
        private System.Windows.Forms.TextBox d;
        private System.Windows.Forms.Button reseach;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.Label tM;
        private System.Windows.Forms.TextBox M;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
    }
}

