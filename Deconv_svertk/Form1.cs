﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using System.Drawing.Drawing2D;

namespace Deconv_svertk
{
    public partial class Form1 : Form
    {
        Model model;
        Pen PenRed, PenSetka, PenBlack, PenBlue;

        double[] x;
        double[] rxx;
        double[] lamb;
        double[] cordR;

        int i = 0;
        int t = 0; //параметр счетчика

        public Form1()
        {
            InitializeComponent();
            PenRed = new Pen(Color.Red, 2);
            PenBlue = new Pen(Color.Blue, 2);
            PenSetka = new Pen(Color.LightGray, 1);
            PenBlack = new Pen(Color.Black, 2);
       
        }
        private void InitializeModel()
        {
            model = new Model();
            model.Setw(w1, w2);
            model.Setф(ф1, ф2);
            model.Setd(d);
            model.SetM(M);
            x = new double[model.GetN()];
            rxx = new double[model.GetM()];
            lamb = new double[model.GetM()];
            cordR = new double[Convert.ToInt16(Tnmax.Text)];
        }


    

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            if (model == null) return;

        }

        private void Draw_Click(object sender, EventArgs e)
        {
            InitializeModel();
           
            model.Signal();

            reseach.Enabled = true;

            x = model.GetX();
            rxx = model.GetRxx();
            lamb = model.GetLamb();
         

            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();
            chart3.Series[0].Points.Clear();

            for (int i = 0; i < model.GetN(); i++)
            {
                chart1.Series[0].Points.AddXY(i, x[i]);
          
               
            }
            for (int i = 0; i < model.GetM(); i++)
            {
               chart2.Series[0].Points.AddXY(i, rxx[i]);
               chart3.Series[0].Points.AddXY(i, lamb[i]);
            }
            bar.Items[1].Text = "л min = " + String.Format("{0:0.0000}", lamb[model.GetM() - 1]) + "    ||    ";
            bar.Items[0].Text = "л max = " + String.Format("{0:0.0000}", lamb[0]) + "    ||    ";
            bar.Items[2].Text = "cond(R) = " + String.Format("{0:0.0000}", model.GetcordR());
            bar.Items[3].Text = "";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {


           
            
         //   t++;

           
          //  if(t*5 > nmax) Reseach_timer.Enabled = false;




          // bar.Items[0].Text = "востановление в процессе... " + "    ||    ";
         //  bar.Items[1].Text = "квадрат отклонений = " + String.Format("{0:0.0000}", model.Getbuffer()) + "    ||    ";
         //  bar.Items[2].Text = "функционал = " + String.Format("{0:0.0000}", model.Getfunc()) + "    ||    ";
         //  bar.Items[3].Text = "счетчик итераций : " + String.Format("{0:0}", iter);
        //    if (model.Finish()){ timer1.Enabled = false;  bar.Items[0].Text = "востановление выполнено "  + "    ||    ";}


        }

        private void vost_Click(object sender, EventArgs e)
        {
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void reseach_Click(object sender, EventArgs e)
        {
           // Reseach_timer.Enabled = true;
            reseach.Enabled = false;

            i = 0;
            int nmax = Convert.ToInt16(Tnmax.Text); // максимальный уровень шума
           


            if (nmax < 500) { chart4.ChartAreas[0].Axes[0].Interval = 10;  } 
            if (nmax >= 500 && nmax<1000) { chart4.ChartAreas[0].Axes[0].Interval = 50; }
            if (nmax >= 1000) { chart4.ChartAreas[0].Axes[0].Interval = 100; }
            try
            {
                for (int t = 0; t < nmax; t++)
                {

                    d.Text = Convert.ToString(t);
                    model.Setd(d);
                    model.Signal();
                    cordR[i] = 1 / model.GetcordR();
                    i++;


                }



                chart4.Series[0].Points.Clear();

                for (int k = 0; k < nmax; k++)
                {
                    chart4.Series[0].Points.AddXY(k, cordR[k]);


                }



            }
            catch
            {
                MessageBox.Show("Выход за пределы массива", "ошибка");
            }
        }

        private void chart4_Click(object sender, EventArgs e)
        {

        }


    }
}
